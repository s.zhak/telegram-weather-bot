package org.example.szhak;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.szhak.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@Component
@Slf4j
public class Bot extends TelegramLongPollingBot {
    @Value("${bot.name}")
    @Getter
    private String botUsername;

    @Value("${bot.token}")
    @Getter
    private String botToken;

    private final WeatherService weatherService;

    @Autowired
    public Bot(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        Location location = update.getMessage().getLocation();
        String answer;
        if (location != null) {
            try {
                answer = weatherService.getWeatherReportForCoords(location.getLatitude(), location.getLongitude()).get();
            } catch(IOException | ExecutionException | InterruptedException e) {
                answer = "Something gone wrong!";
                if (Thread.interrupted())
                    throw new InterruptedException();
            }
        } else {
            String messageText = update.getMessage().getText();
            if (StringUtils.isEmpty(messageText)) {
                return;
            }
            if (messageText.startsWith(("!city"))) {
                String cityName = messageText.substring(6);
                try {
                    answer = weatherService.getWeatherReportForCity(cityName).get();
                } catch (IOException | ExecutionException | InterruptedException e) {
                    answer = "Something went wrong!";
                    if (Thread.interrupted())
                        throw new InterruptedException();
                }
            } else {
                answer = "Enter \"!city yourcity\" or share your location with me to get yourself a report!";
            }
        }
        try {
            execute(new SendMessage().setChatId(update.getMessage().getChatId())
                    .setText(answer));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}