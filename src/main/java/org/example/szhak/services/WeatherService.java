package org.example.szhak.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.example.szhak.pojo.WeatherPojo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class WeatherService {

    @Value("${owm.token}")
    private String token;

    private final ObjectMapper om = new ObjectMapper();

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getName());
    }

    @Async
    public CompletableFuture<String> getWeatherReportForCity(String city) throws IOException {
        URL jsonURL = new URL(String.format("http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s", URLEncoder.encode(city), token));
        log.info("URL composed: {}", jsonURL);
        WeatherPojo report = om.readValue(jsonURL, WeatherPojo.class);
        return CompletableFuture.completedFuture(
                String.format("%s (%s:%s)%n", report.name, report.coord.lon, report.coord.lat) +
                String.format("Current temp: %s%n", (report.main.temp - 273)) +
                String.format("Feels like: %s%n", (report.main.feels_like - 273))
        );
    }

    @Async
    public CompletableFuture<String> getWeatherReportForCoords(Float lat, Float lon) throws IOException {
        URL jsonURL = new URL(String.format("http://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s", lat, lon, token));
        log.info("URL composed: {}", jsonURL);
        WeatherPojo report = om.readValue(jsonURL, WeatherPojo.class);
        return CompletableFuture.completedFuture(
                String.format("%s (%s:%s)%n", report.name, report.coord.lon, report.coord.lat) +
                        String.format("Current temp: %s%n", (report.main.temp - 273)) +
                        String.format("Feels like: %s%n", (report.main.feels_like - 273))
        );
    }
}
